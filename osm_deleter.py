# Handling command line options
import sys
import argparse
import math
from multiprocessing import Pool

# Parsing the maps
import xml.etree.ElementTree as et

# Parsing the config file
import json

desc_string = """
Stripper for .OSM files.
This script removes alls relations that don't have a \"type\" tag matching the list defined in the configuration file.
All rm_ways are removed except for those with the \"k\" value \"highways\" with a type matching the list in the configuration file.
"""

parser = argparse.ArgumentParser(description=desc_string)
parser.add_argument('-i', '--input')

args = parser.parse_args()

input_path = args.input
print("File: ", input_path+".osm")
print("Will write to: ", input_path+"deleted.osm")

try:
    tree = et.parse(input_path+".osm")
except FileNotFoundError:
    print("Map file not found, check the filename and try again")
    exit()

root = tree.getroot()

del_count = 0
to_rem = []
for r in root:
    if "action" in r.attrib.keys():
        del_count += 1
        to_rem.append(r)
        if del_count % 10000 == 0:
            print(del_count)

for r in to_rem:
    root.remove(r)

tree.write(input_path+"deleted.osm")

