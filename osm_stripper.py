# Handling command line options
import sys
import argparse
import math
from multiprocessing import Pool

# Parsing the maps
import xml.etree.ElementTree as et

# Parsing the config file
import json

desc_string = """
Stripper for .OSM files.
This script removes alls relations that don't have a \"type\" tag matching the list defined in the configuration file.
All rm_ways are removed except for those with the \"k\" value \"highways\" with a type matching the list in the configuration file.
"""

parser = argparse.ArgumentParser(description=desc_string)
parser.add_argument('-i', '--input')
parser.add_argument('-o', '--output')
parser.add_argument('-c', '--config')

args = parser.parse_args()

config_file_path = args.config if args.config else "config.json"
input_path = args.input
output_path = args.output

try:
    tree = et.parse(input_path)
except FileNotFoundError:
    print("Map file not found, check the filename and try again")
    exit()
try:
    f = open(config_file_path)
    config = json.load(f)["config"]
except FileNotFoundError:
    print("Configuration file not found, check the filename and try again. The default filename is \"config.json\"")
    exit()

rm_ways = config["ways"]
rm_relations = config["relations"]

# Convenience functions
def is_highway(way):
    for member in get_tagged_subelements(way, "tag"):
        if member.attrib["k"] == "highway":
            return True
    return False

def get_tagged_subelements(element, tag):
    return [i for i in element if i.tag == tag] 

def get_node_with_ref(root, reference):
    a = [i for i in root if i.tag == "node" and i.attrib["id"] == reference]
    if not a:
        return None
    else:
        return a[0]

def get_distance_between_nodes(a, b):
    a_lat = float(a.attrib["lat"])
    a_lon = float(a.attrib["lon"])
    b_lat = float(b.attrib["lat"])
    b_lon = float(b.attrib["lon"])
    return math.sqrt((abs(a_lat - b_lat) ** 2) + abs(a_lon - b_lon) ** 2)

def get_road_using_node(root, search_node):
    for way in road_ways:
        for node in get_tagged_subelements(way, "nd"):
            if node.attrib["ref"] == search_node.attrib["id"]:
                return way
    return None

def get_object_with_id(root, id):
    for i in root:
        if "id" in i.attrib.keys() and i.attrib["id"] == id:
            return i
    return None

def get_tagged_key_val(elem, key):
    for i in get_tagged_subelements(elem, "tag"):
        if i.attrib["k"] == key:
            return i.attrib["v"]
    return None

def get_tagged_key_element(parent, key):
    for i in get_tagged_subelements(parent, "tag"):
        if i.attrib["k"] == key:
            return i
    return None


def is_building(way):
    for member in get_tagged_subelements(way, "tag"):
        if member.attrib["k"] == "building":
            return True
    return False

# Get some important global variables, saves a lot of time searching large XML files
ways = tree.getroot().findall("./way")
relations = tree.getroot().findall("./relation")
nodes = tree.getroot().findall("./node")

for node in nodes:
    lat_element = et.Element("tag")
    lat_element.attrib["k"] = "lat"
    lat_element.attrib["v"] = str(node.attrib["lat"])
    node.append(lat_element)
    lon_element = et.Element("tag")
    lon_element.attrib["k"] = "lon"
    lon_element.attrib["v"] = str(node.attrib["lon"])
    node.append(lon_element)

# filter the list of ways to isolate those which are highways
road_ways = []
for i in ways:
    if is_highway(i):
        road_ways.append(i)

ways_to_remove = []
for way in road_ways:
    for node in get_tagged_subelements(way, "nd"):
        if not get_node_with_ref(tree.getroot(), node.attrib['ref']):
            if way not in ways_to_remove:
                ways_to_remove.append(way)

for way in ways_to_remove:
    tree.getroot().remove(way)

# build the list of road nodes to save recomputation
# This is quite expensive and time consuming
road_nodes = []
for way in road_ways:
    for node in get_tagged_subelements(way, "nd"):
        if node not in road_nodes:
            road_nodes.append(get_node_with_ref(tree.getroot(), node.attrib["ref"]))

print("There are: ", len(ways), " ways")
print("There are: ", len(road_ways), " road ways")
print("There are: ", len(relations), " relations")
print("There are: ", len(nodes), " nodes")
print("There are: ", len(road_nodes), " road nodes")

# Iterate over all roads, calculating their length
# Set up the measures for the road density algorithm
for way in road_ways:
    nodes = get_tagged_subelements(way, "nd")
    total_distance = 0
    for i in range(0, len(nodes) - 1):
        total_distance += get_distance_between_nodes(get_node_with_ref(tree.getroot(), nodes[i].attrib["ref"]),
                                              get_node_with_ref(tree.getroot(), nodes[i+1].attrib["ref"]))
    if(total_distance == 0):
        import pdb; pdb.set_trace()
    road_length = et.Element("tag")
    road_length.attrib["k"] = "road_length"
    road_length.attrib["v"] = str(total_distance)
    way.append(road_length)
    no_buildings = et.Element("tag")
    no_buildings.attrib["k"] = "no_buildings"
    no_buildings.attrib["v"] = str(0)
    way.append(no_buildings)
    building_density = et.Element("tag")
    building_density.attrib["k"] = "building_density"
    building_density.attrib["v"] = str(1.0)
    way.append(building_density)

def process_way(way):
    if is_building(way):
        # Found a building, choose one of the nodes and work out which street it is nearest
        se = get_tagged_subelements(way, "nd")
        if not se:
            return
        else:
            node = get_node_with_ref(tree.getroot(), se[0].attrib["ref"])
            best_node = None
            best_distance = None
            for candidate_node in road_nodes:
                distance = get_distance_between_nodes(candidate_node, node)
                if not best_distance or distance < best_distance:
                    best_distance = distance
                    best_node = candidate_node
            # Now we have the nearest road node, find the road it's on, and increment the building count for that road
            nearest_road = get_road_using_node(tree.getroot(), best_node)
            get_tagged_key_element(nearest_road, "no_buildings").attrib["v"] = str(int(get_tagged_key_val(nearest_road, "no_buildings")) + 1)
            get_tagged_key_element(nearest_road, "building_density").attrib["v"] = str(int(get_tagged_key_val(nearest_road, "no_buildings")) / float(get_tagged_key_val(nearest_road, "road_length")))

## Add a building density measure to roads
# This maintains a dictionary of buildings, tagged with the road that they are nearest
print("Processing ways for BD")
for i in range(0, len(ways)):
    print("On way: ", i, " of ", len(ways))
    process_way(ways[i])


######### Now strip the file to the elements that are specified in the config

# First remove relations that aren't of interest
for child in [i for i in tree.getroot() if i.tag == 'relation']:
    to_del = False
    for member in [i for i in child if i.tag == "tag"]:
        if member.attrib["k"] == "type" and member.attrib["v"] in rm_relations:
            to_del = True
    if to_del:
        tree.getroot().remove(child)

# Remove rm_ways we're not interested in
for child in [i for i in tree.getroot() if i.tag == 'way']:
    to_delete = True
    for member in [i for i in child if i.tag == "tag"]:
        if member.attrib["k"] == "highway" and member.attrib["v"] in rm_ways:
            to_delete = False
    if to_delete:
        tree.getroot().remove(child)

# Prune non-required nodes
required_node_list = list()
for child in [i for i in tree.getroot() if i.tag == 'way']:
    for member in [i for i in child if i.tag == "nd"]:
        required_node_list.append(member.attrib["ref"])

for child in [i for i in tree.getroot() if i.tag == 'node']:
    if child.attrib["id"] not in required_node_list:
        tree.getroot().remove(child)


tree.write(output_path)

