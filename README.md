# OSM Stripper

A script removing unnecessary data from an OSM map, as well as calculating the building density of roads.

`osm_deleter.py` actions delete commands as annotated by the JOSM editor.

`osm_stripper.py` performs the removal of all buildings, relations, non-road ways and other components.
Those components that remain are only the specified roads.

`osm_stripper.py` takes three arguments, one of which is optional. Use `python3 osm_stripper.py --help` to see an explanation of the options.